package com.ostapchuk.model.bdemo;

import com.ostapchuk.model.Numbers;
import com.ostapchuk.model.PrintArray;
import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;
import java.util.*;

//B. Видалити в масиві всі числа, які повторюються більше двох разів.

public class StartB {
    private List<Integer> arrayNums = new Numbers(3).getNumber();

    public void UI() {
        View view = new ViewGeneral();
        printArray(arrayNums);
        view.justPrint("Array, after the program deleted numbers that appear in array more than twice:");
        arrayNums = deleteSameNums(arrayNums);
        printArray(arrayNums);
    }

    public List<Integer> deleteSameNums(List<Integer> array) {
        List<Integer> tempArray = array;
        List<Integer> toDelete = new LinkedList<>();
        for (int i = 0; i < tempArray.size(); i++) {
            for (int j = 0; j < tempArray.size(); j++) {
                if (tempArray.get(i).equals(tempArray.get(j))) {
                    toDelete.add(i);
                    toDelete.add(j);
                }
            }
        }
        for (Integer i : toDelete) {
            tempArray.remove(i);
        }
        return tempArray;
    }

    public void printArray(List<Integer> array) {
        new PrintArray(array);
    }

    public List<Integer> getArrayNums() {
        return arrayNums;
    }
}
