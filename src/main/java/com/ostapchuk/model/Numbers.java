package com.ostapchuk.model;

import com.ostapchuk.view.ViewGeneral;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class will store the numbers from txt file.
 * In the start of the program object of this class will be initialized and
 * passed to different classes that will work with it.
 */

public class Numbers {
    private static final int ARRAYLENGTH = 7;
    private List<Integer> number;

    public Numbers(int i) {
        number = new ArrayList(ARRAYLENGTH);
        String fileName = "E:\\EPAM\\task04_Arrays\\src\\main\\resources\\numbers" + i + ".txt";

        String line;

        try (BufferedReader bufferedReader =
                     new BufferedReader(new FileReader(fileName))) {
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals("") || line.equals("\n") || line.equals(" ")) {
                    continue;
                }
                number.add(Integer.parseInt(line));
            }
        } catch (FileNotFoundException ex) {
            new ViewGeneral().justPrint("Unable to open file '" + fileName + "'");
        } catch (IOException ex) {
            new ViewGeneral().justPrint("Error reading file '" + fileName + "'");
        }
    }

    public static int getARRAYLENGTH() {
        return ARRAYLENGTH;
    }

    public List getNumber() {
        return number;
    }
}
