package com.ostapchuk.model.ddemo;

import com.ostapchuk.model.ddemo.characters.Entity;

import java.util.AbstractCollection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LimitedCollection<E extends Entity> extends AbstractCollection {
    private Entity[] collection;
    private int maxSize;
    private int size;

    public LimitedCollection() {
        this.maxSize = 10;
        this.size = 0;
        collection = new Entity[this.maxSize];
    }

    @Override
    public Iterator iterator() {
        Iterator iterator = new IteratorForLimited();
        ((IteratorForLimited) iterator).currentIndex = 0;
        return iterator;
    }

    @Override
    public int size() {
        return size;
    }

    //todo fix this
    public boolean addEntity(E e) {
        collection[size] = e;
        size++;
        return true;
    }

    public Entity get(int index) throws ArrayIndexOutOfBoundsException {
        if(index < 0 | index > 9) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return collection[index];
    }

    public class IteratorForLimited<E extends Entity> implements Iterator {
        private int currentIndex;

        public boolean hasNext() {
            if (currentIndex >= size()) {
                return false;
            } else {
                return true;
            }
        }

        public Entity next() {
            if(!hasNext()) {
                throw new NoSuchElementException();
            }
            return collection[currentIndex++];
        }
    }
}
