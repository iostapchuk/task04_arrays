package com.ostapchuk.model.ddemo;

import com.ostapchuk.model.ddemo.characters.Artifact;
import com.ostapchuk.model.ddemo.characters.Entity;
import com.ostapchuk.model.ddemo.characters.Hero;
import com.ostapchuk.model.ddemo.characters.Monster;
import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Level {
    private LimitedCollection<Entity> doors;
    private View view;
    private Random rand;
    private Hero hero;
    private Random randomNumber;

    public Level() {
        this.doors = new LimitedCollection<Entity>();
        view = new ViewGeneral();
        rand = new Random();
        this.hero = new Hero();
        this.randomNumber = new Random();
    }

    public void fillLevelRandom() {
        //getRandomNumberInRange(1, 2);
        for (int i = 0; i < 10; i++) {
            int n = getRandomNumberInRange(1, 2);
            if (n == 1) {
                addRandomMonster();
            } else if (n == 2) {
                addRandomArtifact();
            } else {
                view.justPrint("Something bad happened.");
            }
        }
    }

    public void goodWay() {
        ArrayList<Integer> way = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (doors.get(i).getType().equals("Artifact")) {
                hero.addStrength(doors.get(i).getStrength());
                way.add(i);
            }
        }
        for(int i = 0; i < 10; i++) {
            if (doors.get(i).getType().equals("Monster")) {
                if(doors.get(i).getStrength() > hero.getStrength()) {
                    noGoodWay();
                    return;
                } else {
                    way.add(i);
                }
            }
        }
        printGoodWay(way);
    }

    private void noGoodWay() {
        view.justPrint("There is no safe way for the hero.");
    }

    private void printGoodWay(ArrayList<Integer> way) {
        view.justPrint("Hero managed to first get all the artifacts and the defeated all monsters!");
        view.justPrint("Here is how he did it:");
        view.justPrint("He chose doors in this order ");
        for (Integer i : way) {
            view.justPrint("Door " + i);
        }
        view.justPrint("Hero's strength now is " + hero.getStrength() + "!");
    }

    public void addMonster(int strength) {
        if (strength < 5 | strength > 100) {
            view.justPrint("Wrong strength value." +
                    "\nFor monsters its from 5 to 100.");
        } else {
            Monster monster = new Monster(strength);
            doors.addEntity(monster);
        }
    }

    public void addArtifact(int strength) {
        if (strength < 10 | strength > 80) {
            view.justPrint("Wrong strength value." +
                    "\nFor artifacts its from 5 to 100.");
        } else {
            Artifact artifact = new Artifact(strength);
            doors.addEntity(artifact);
        }
    }

    public void printLevel() {
        if (doors.isEmpty()) {
            view.justPrint("Level is empty!");
            return;
        }
        Iterator iterator = doors.iterator();
        view.justPrint("|Door number |    Type    |  Strength  |");
        int tabLength = 12; //length of every row in table
        for (int i = 0; i < 10; i++) {
            Entity entity = (Entity) iterator.next();
            view.justPrint("|" + getTabulatedString(i, tabLength) +
                    getTabulatedString(entity.getType(), tabLength) +
                    getTabulatedString(entity.getStrength(), tabLength));
        }
    }

    public void recursiveDeath() {
        int count = recursive(0, 0);
        String message = "Death awaits the Hero behind " + count + " ";
        if (count == 1) {
            message += "door";
        } else {
            message += "doors";
        }
        view.justPrint(message);
    }

    private int recursive(int deathCount, int doorCount) {
        int actualCount = deathCount;
        if (deathDoor(doorCount)) {
            actualCount = actualCount + 1;
        }
        if (doorCount < 10) {
            actualCount = recursive(actualCount, doorCount + 1);
        }
        return actualCount;
    }

    private boolean deathDoor(int doorNumber) {
        Entity behindDoor;
        try {
            behindDoor = doors.get(doorNumber);
        } catch (IndexOutOfBoundsException e) {
            e.getMessage();
            return false;
        }
        if (behindDoor.getType() == "Monster") {
            if (behindDoor.getStrength() > hero.getStrength()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param string
     * @param tabLength - length in characters of row of text table that is formed using this method
     * @return
     */
    private String getTabulatedString(String string, int tabLength) {
        String tabulated = "" + string;
        while (tabulated.length() < tabLength) {
            tabulated += " ";
        }
        return tabulated + "|";
    }

    private String getTabulatedString(int number, int tabLength) {
        String tabulated = "" + number;
        while (tabulated.length() < tabLength) {
            tabulated += " ";
        }
        return tabulated + "|";
    }

    private void addRandomArtifact() {
        Artifact artifact = new Artifact((rand.nextInt(70) + 10));
        doors.addEntity(artifact);
    }

    private void addRandomMonster() {
        Monster monster = new Monster((rand.nextInt(95) + 5));
        doors.addEntity(monster);
    }

    private int getRandomNumberInRange(int min, int max) {
        return randomNumber.ints(min, (max + 1)).limit(1).findFirst().getAsInt();
    }
}
