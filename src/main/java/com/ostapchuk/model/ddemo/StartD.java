package com.ostapchuk.model.ddemo;

import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;

/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів, знаходиться в круглому залі,
 *      з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт, що дарує силу від 10 до 80 балів,
 *      або монстр, який має силу від 5 до 100 балів, з яким герою потрібно битися.
 *      Битву виграє персонаж, що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 *
 * 1. Організувати введення інформації про те, що знаходиться за дверима, або заповнити її,
 *      використовуючи генератор випадкових чисел.
 *
 * 2. Вивести цю саму інформацію на екран в зрозумілому табличному вигляді.
 *
 * 3. Порахувати, за скількома дверима героя чекає смерть. Рекурсивно.
 *
 * 4. Вивести номери дверей в тому порядку, в якому слід їх відкривати герою, щоб залишитися в живих, якщо таке можливо.
 */
public class StartD {
    View view = new ViewGeneral();
    Level level;

    public void UI() {
        levelGenerationChoice();
        level.printLevel();
        level.recursiveDeath();
        level.goodWay();
    }

    private void levelGenerationChoice() {
        view.justPrint("Choose an option:" +
                "\n1. Generate a random level." +
                "\n2. Generate a level by hand.");
        int choice = getNumber(1, 2);
        if(choice == 1) {
            generateRandomLevel();
        } else if(choice == 2) {
            generateLevelByHand();
        } else {
            view.justPrint("Something wrong after number input.");
        }
    }

    private void generateRandomLevel() {
        level = new Level();
        level.fillLevelRandom();
    }

    private void generateLevelByHand() {
        level = new Level();
        for(int i = 0; i < 10; i++) {
            view.justPrint("Choose an option:" +
                    "\n1. Generate an Artifact." +
                    "\n2. Generate a Monster.");
            int choice = getNumber(1, 2);
            if(choice == 1) {
                view.justPrint("Choose an Artifact`s strength from 10 to 80");
                level.addArtifact(getNumber(10, 80));
            } else if(choice == 2){
                view.justPrint("Choose a Monster`s strength from 5 to 100");
                level.addMonster(getNumber(5, 100));
            } else {
                //todo delete after testing
                view.justPrint("wtf");
            }
        }
    }

    private int getNumber(int lower, int higher) {
        int choice = -1;
        while (true) {
            choice = view.getInt();
            if (!view.checkNumber(choice, lower, higher)) {
                view.justPrint("Number is not between " + lower + " and " + higher + ".");
                view.tryAgain();
                continue;
            }
            break;
        }
        return choice;
    }
}
