package com.ostapchuk.model.ddemo.characters;

public abstract class Entity {
    public abstract int getStrength();
    public abstract String getType();
}
