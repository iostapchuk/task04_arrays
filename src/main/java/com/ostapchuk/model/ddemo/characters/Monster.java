package com.ostapchuk.model.ddemo.characters;

public class Monster extends Entity{
    private int strength;

    public Monster(int str) {
        this.strength = str;
    }

    public int getStrength() {
        return strength;
    }

    public String getType() {
        return "Monster";
    }
}
