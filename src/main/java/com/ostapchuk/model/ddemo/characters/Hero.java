package com.ostapchuk.model.ddemo.characters;

public class Hero {
    private int strength;

    public Hero() {
        this.strength = 25;
    }

    public int getStrength() {
        return strength;
    }

    public void addStrength(int add) {
        this.strength += add;
    }
}
