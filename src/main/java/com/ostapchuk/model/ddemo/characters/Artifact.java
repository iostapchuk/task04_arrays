package com.ostapchuk.model.ddemo.characters;

public class Artifact extends Entity{
    private int strength;

    public Artifact(int str) {
        this.strength = str;
    }

    public int getStrength() {
        return strength;
    }

    public String getType() {
        return "Artifact";
    }
}
