package com.ostapchuk.model.ademo;

/*
A. Дано два масиви. Сформувати третій масив, що складається з тих елементів, які:
а) присутні в обох масивах;
б) присутні тільки в одному з масивів.
 */

import com.ostapchuk.model.PrintArray;
import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;
import com.ostapchuk.model.Numbers;

import java.util.List;

public class StartA {
    View view = new ViewGeneral();

    List<Integer> firstArray;
    List<Integer> secondArray;

    public StartA(Numbers numbers, Numbers numbers2) {
        this.firstArray = numbers.getNumber();
        this.secondArray = numbers2.getNumber();
    }

    public void UI() {
        printFirst();
        printSecond();
        view.justPrint("Numbers in either arrays:");
        printEitherNumbers();
        view.justPrint("Only from first array:");
        printOnlyFirst(firstArray, secondArray);
        view.justPrint("Only from second array:");
        printOnlyFirst(secondArray, firstArray);
    }

    public void printFirst() {
        new PrintArray(firstArray);
    }

    public void printSecond() {
        new PrintArray(secondArray);
    }

    public void printArray(int[] array){
        view.justPrint("Array:");
        String s = "";
        for(int i : array) {
            s += " " + i;
        }
        view.justPrint(s);
    }

    public void printEitherNumbers() {
        int[] tempSameNums = getTempArray(firstArray, secondArray);
        int countPosition = 0;
        for(int i = 0; i < firstArray.size(); i++) {
            for(int j = 0; j < secondArray.size(); j++){
                if(firstArray.get(i).equals(secondArray.get(j))) {
                    if (isInArray(secondArray.get(j), tempSameNums)) {
                        continue;
                    }
                    tempSameNums[countPosition] = secondArray.get(j);
                    countPosition++;
                }
            }
        }
        int[] actualSameNums = getActualArray(countPosition, tempSameNums);
        printArray(actualSameNums);
    }

    public void printOnlyFirst(List<Integer> first, List<Integer> second){
        int[] tempSameNums = getTempArray(first, second);
        int countPosition = 0;
        for(int i = 0; i < first.size(); i++) {
            boolean isInSecond = false;
            for( int j = 0; j < second.size(); j++) {
                if(first.get(i).equals(second.get(j))) {
                    isInSecond = true;
                }
            }
            if(!isInSecond) {
                tempSameNums[countPosition] = (int) first.get(i);
                countPosition++;
            }
        }
        int[] actualArray = getActualArray(countPosition, tempSameNums);
        view.justPrint("Only in this array:");
        printArray(actualArray);
    }

    private int[] getActualArray(int countPosition, int[] tempArray) {
        int[] actualSameNums = new int[countPosition];
        for(int i = 0; i < actualSameNums.length; i++) {
            actualSameNums[i] = tempArray[i];
        }
        return actualSameNums;
    }

    private int[] getTempArray(List<Integer> first, List<Integer> second){
        int maxLength = first.size();
        if(second.size() > first.size()) {
            maxLength = second.size();
        }
        int[] tempSameNums = new int[maxLength];
        return tempSameNums;
    }

    private boolean isInArray(int checkMe, int[] array){
        for(int i = 0; i < array.length; i++) {
            if (checkMe == array[i]) {
                return true;
            }
        }
        return false;
    }
}
