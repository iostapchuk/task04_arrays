package com.ostapchuk.model.cdemo;

import com.ostapchuk.model.Numbers;
import com.ostapchuk.model.PrintArray;
import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;

import java.util.List;

/**
 * C. Знайти в масиві всі серії однакових елементів, які йдуть підряд, і видалити з них всі елементи крім одного.
 */

public class StartC {
    Numbers numbers = new Numbers(4);
    View view = new ViewGeneral();
    List<Integer> array = numbers.getNumber();

    public void UI() {
        printArray(array);
        view.justPrint("Array without series:");
        deleteSeries(array);
        view.justPrint("New array:");
        printArray(array);
    }

    private void deleteSeries() {
        while(true) {
            int start = getSeriesStart(array);
            if(start == -1) {
                break;
            }
            int end = getSeriesEnd(array, start);
            deleteSeries(array, start, end);
        }
    }

    private void deleteSeries(List<Integer> array, int start, int end) {
        for(int i = end; i > start; i--) {
            array.remove(i);
        }
    }

    private void deleteSeries(List<Integer> array) {
        int current;
        for(int i = 0; i < array.size(); i++) {
            current = array.get(i);
            for(int j = i +1; j < array.size(); ) {
                if(array.get(j) == current) {
                    array.remove(j);
                } else {
                    break;
                }
            }
        }
    }

    private int getSeriesStart(List<Integer> array) {
        int start = -1;
        for(int i = 1; i < array.size(); i++) {
            if(array.get(i - 1).equals(array.get(i))) {
                start = i - 1;
                i++;
            }
        }
        return start;
    }

    private int getSeriesEnd(List<Integer> array, int start) {
        int end = array.size();
        int startNumber = array.get(start);
        for(int i = start; i < array.size(); i++) {
            if(array.get(i) != startNumber) {
                end = i;
            }
        }
        return end - 1;
    }

    public void printArray(List<Integer> array) {
        new PrintArray(array);
    }
}
