package com.ostapchuk.model;

import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;

import java.util.List;

public class PrintArray {
    View view = new ViewGeneral();

    public PrintArray(List<Integer> array) {
        new ViewGeneral().justPrint("Array:");
        String s = "";
        for (int i : array) {
            s += " " + i;
        }
        new ViewGeneral().justPrint(s);
    }
}
