package com.ostapchuk.controller;

public interface Controller {
    void seeTaskA();
    void seeTaskB();
    void seeTaskC();
    void seeTaskD();
    void start();
}
