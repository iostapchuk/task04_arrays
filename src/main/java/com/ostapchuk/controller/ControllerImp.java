package com.ostapchuk.controller;

import com.ostapchuk.model.Numbers;
import com.ostapchuk.model.cdemo.StartC;
import com.ostapchuk.model.ddemo.StartD;
import com.ostapchuk.view.View;
import com.ostapchuk.view.ViewGeneral;
import com.ostapchuk.model.ademo.StartA;
import com.ostapchuk.model.bdemo.StartB;

public class ControllerImp implements Controller{
    private View view = new ViewGeneral();
    Numbers numbers1 = new Numbers(1);
    Numbers numbers2 = new Numbers(2);



    public void start() {
        view.start();
    }

    public void seeTaskA() {
        StartA startA = new StartA(numbers1, numbers2);
        startA.UI();
    }

    public void seeTaskB() {
        StartB startB = new StartB();
        startB.UI();
    }

    public void seeTaskC() {
        StartC startC = new StartC();
        startC.UI();
    }

    public void seeTaskD() {
        StartD startD = new StartD();
        startD.UI();
    }
}
