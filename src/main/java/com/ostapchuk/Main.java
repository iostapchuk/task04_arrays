package com.ostapchuk;

import com.ostapchuk.controller.Controller;
import com.ostapchuk.controller.ControllerImp;

public class Main {
    public static void main(String[] args) {
        Controller controller = new ControllerImp();
        controller.start();
    }
}
