package com.ostapchuk.view;

import com.ostapchuk.controller.Controller;
import com.ostapchuk.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ViewGeneral implements View {
    private static final Logger logger1 =
            LogManager.getLogger(ViewGeneral.class);
    private Controller controller;
    private int lowerMenuChoice = 0;
    private int higherMenuChoice = 5;

    public void start() {
        controller = new ControllerImp();
        while (true) {
            logger1.info(StringsBase.menu);
            int choice = -1;
            while (true) {
                printNumber(lowerMenuChoice, higherMenuChoice);
                choice = getInt();
                if (!checkNumber(choice, lowerMenuChoice, higherMenuChoice)) {
                    tryAgain();
                    continue;
                } else break;
            }
            if (choice == 0) {
                logger1.fatal("choice == 0, Terminating.");
                return;
            } else if (choice == 5) {
                logger1.info("Exiting...");
                break;
            }
            seeTask(choice);
        }
    }

    //todo make program work without this
    public void justPrint(String s) {
        logger1.info(s);
    }

    private void seeTask(int taskNumber) {
        printYouChose(taskNumber);
        switch (taskNumber) {
            case 1:
                controller.seeTaskA();
                break;
            case 2:
                controller.seeTaskB();
                break;
            case 3:
                controller.seeTaskC();
                break;
            case 4:
                controller.seeTaskD();
                break;
        }
    }

    private void printYouChose(int taskNumber) {
        char taskChar = 'A';
        if (taskNumber == 2) {
            taskChar = 'B';
        } else if (taskNumber == 3) {
            taskChar = 'C';
        } else if (taskNumber == 4) {
            taskChar = 'D';
        }
        logger1.info(StringsBase.youChose + "task " + taskChar);
    }

    public int getInt() {
        int numberNeeded = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String numberString = br.readLine();
                if (numberString.isBlank() | numberString.isEmpty()) {
                    continue;
                }
                numberNeeded = Integer.parseInt(numberString);
                logger1.info(StringsBase.operationGood);
                break;
            } catch (IOException e) {
                logger1.debug(StringsBase.operationBad);
                e.printStackTrace();
            } finally {
                logger1.info(StringsBase.operationFinished);
            }
        }
        return numberNeeded;
    }

    private void printNumber(int lower, int higher) {
        logger1.info(StringsBase.numberNeeded + " between " + lower + " and " + higher);
    }

    public boolean checkNumber(int choice, int lower, int higher) {
        if (choice < lower) return false;
        else if (choice > higher) return false;
        else return true;
    }

    public void tryAgain() {
        logger1.info(StringsBase.wrongTryAgain);
    }
}
