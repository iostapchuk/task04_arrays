package com.ostapchuk.view;

public interface View {
    void start();
    void justPrint(String s);
    int getInt();
    boolean checkNumber(int choice, int lower, int higher);
    void tryAgain();
}
