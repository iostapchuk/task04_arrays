package com.ostapchuk.view;

public class StringsBase {
    static String menu =    "\n1. Look at task A;\n" +
                            "2. Look at task B;\n" +
                            "3. Look at task C;\n" +
                            "4. Look at task D;\n" +
                            "5. Exit.\n";

    static String numberNeeded = "Please, put in number";

    static String operationGood = "Operation successful.";

    static String operationBad = "Operation failed.";

    static String operationFinished = "Operation finished.";

    static String wrongTryAgain = "Try again.";

    static String youChose = "You chose to see ";
}
